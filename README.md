# Guías e Infografías

Repositorio de guías en infografías producidas por el [Colectivo Disonancia](colectivodisonancia.net).



## Internet Archive

Algunas de estos contenidos también están disponibles en nuestra cuenta de [Internet Archive](https://archive.org/details/@colectivo_disonancia).



## Licencia

Contenidos bajo licencia [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.es_ES), exceptuando aquellos contenidos que indiquen lo contrario.

El texto de la licencia se encuentra en [Licence](https://gitlab.com/cdisonancia/graficas_cd/-/blob/master/LICENSE).

